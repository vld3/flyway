--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2 (Debian 13.2-1.pgdg100+1)
-- Dumped by pg_dump version 13.2 (Debian 13.2-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: dict_album_format; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.dict_album_format (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    album_format_number character varying(50) NOT NULL,
    effective_date date NOT NULL
);



--
-- Name: dict_declaration_mode; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.dict_declaration_mode (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    mode_identity character varying(50) NOT NULL,
    mode_description character varying(255) NOT NULL
);



--
-- Name: dict_exch_spec; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.dict_exch_spec (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    specification_number character varying(50) NOT NULL,
    effective_date date NOT NULL
);



--
-- Name: dict_message; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.dict_message (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    message_system_number character varying(50) NOT NULL,
    message_description character varying(255) NOT NULL,
    message_source_id uuid NOT NULL
);



--
-- Name: dict_message_dict_album_format; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.dict_message_dict_album_format (
    album_format_id uuid NOT NULL,
    message_id uuid NOT NULL
);



--
-- Name: dict_message_dict_exch_spec; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.dict_message_dict_exch_spec (
    message_id uuid NOT NULL,
    exch_spec_id uuid NOT NULL
);



--
-- Name: dict_message_source; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.dict_message_source (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    exchange_direction character varying(50) NOT NULL
);



--
-- Name: dict_proc_identity_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.dict_proc_identity_type (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    identity_type_name character varying(50) NOT NULL
);



--
-- Name: entity_executed_scenario; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.entity_executed_scenario (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    scenario_id uuid NOT NULL,
    last_completed_step_number integer,
    scenario_completed boolean NOT NULL
);



--
-- Name: entity_scenario; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.entity_scenario (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    scenario_name character varying(255) NOT NULL,
    still_valid boolean NOT NULL,
    album_format_id uuid NOT NULL,
    exch_spec_id uuid NOT NULL,
    declaration_mode_id uuid NOT NULL,
    default_scenario_mark boolean NOT NULL
);



--
-- Name: entity_scenario_content; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.entity_scenario_content (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    scenario_id uuid NOT NULL,
    step_num integer NOT NULL,
    message_id uuid NOT NULL,
    changing_proc_identity boolean NOT NULL
);



--
-- Name: entity_scenario_identifier; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.entity_scenario_identifier (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    executed_scenario_id uuid NOT NULL,
    identity_type_id uuid NOT NULL,
    current_procedure_identifier boolean NOT NULL
);



--
-- Name: entity_scenario_message; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.entity_scenario_message (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    executed_scenario_id uuid NOT NULL,
    message_type_id uuid NOT NULL,
    message_time timestamp(0) without time zone NOT NULL,
    message_content character varying(255) NOT NULL,
    step_id uuid
);



--
-- Data for Name: dict_album_format; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.dict_album_format (id, album_format_number, effective_date) FROM stdin;
0e7f1a2e-1a4e-4d87-8564-11478d7e532f	5.17.0	2021-04-01
dc888eb8-ff07-4078-ad4b-3ac3a17157e5	5.18.0	2021-06-16
\.


--
-- Data for Name: dict_declaration_mode; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.dict_declaration_mode (id, mode_identity, mode_description) FROM stdin;
\.


--
-- Data for Name: dict_exch_spec; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.dict_exch_spec (id, specification_number, effective_date) FROM stdin;
a2147102-46f5-4ccb-bbb8-010c9416e11b	3.4.4	2021-06-16
\.


--
-- Data for Name: dict_message; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.dict_message (id, message_system_number, message_description, message_source_id) FROM stdin;
\.


--
-- Data for Name: dict_message_dict_album_format; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.dict_message_dict_album_format (album_format_id, message_id) FROM stdin;
\.


--
-- Data for Name: dict_message_dict_exch_spec; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.dict_message_dict_exch_spec (message_id, exch_spec_id) FROM stdin;
\.


--
-- Data for Name: dict_message_source; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.dict_message_source (id, exchange_direction) FROM stdin;
\.


--
-- Data for Name: dict_proc_identity_type; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.dict_proc_identity_type (id, identity_type_name) FROM stdin;
\.


--
-- Data for Name: entity_executed_scenario; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.entity_executed_scenario (id, scenario_id, last_completed_step_number, scenario_completed) FROM stdin;
\.


--
-- Data for Name: entity_scenario; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.entity_scenario (id, scenario_name, still_valid, album_format_id, exch_spec_id, declaration_mode_id, default_scenario_mark) FROM stdin;
\.


--
-- Data for Name: entity_scenario_content; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.entity_scenario_content (id, scenario_id, step_num, message_id, changing_proc_identity) FROM stdin;
\.


--
-- Data for Name: entity_scenario_identifier; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.entity_scenario_identifier (id, executed_scenario_id, identity_type_id, current_procedure_identifier) FROM stdin;
\.


--
-- Data for Name: entity_scenario_message; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.entity_scenario_message (id, executed_scenario_id, message_type_id, message_time, message_content, step_id) FROM stdin;
\.


--
-- Name: dict_album_format dict_album_format_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dict_album_format
    ADD CONSTRAINT dict_album_format_pk PRIMARY KEY (id);


--
-- Name: dict_declaration_mode dict_declaration_mode_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dict_declaration_mode
    ADD CONSTRAINT dict_declaration_mode_pk PRIMARY KEY (id);


--
-- Name: dict_exch_spec dict_exch_spec_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dict_exch_spec
    ADD CONSTRAINT dict_exch_spec_pk PRIMARY KEY (id);


--
-- Name: dict_message_dict_album_format dict_message_dict_album_format_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dict_message_dict_album_format
    ADD CONSTRAINT dict_message_dict_album_format_pk PRIMARY KEY (message_id, album_format_id);


--
-- Name: dict_message_dict_exch_spec dict_message_dict_exch_spec_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dict_message_dict_exch_spec
    ADD CONSTRAINT dict_message_dict_exch_spec_pk PRIMARY KEY (exch_spec_id, message_id);


--
-- Name: dict_message dict_message_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dict_message
    ADD CONSTRAINT dict_message_pk PRIMARY KEY (id);


--
-- Name: dict_message_source dict_message_source_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dict_message_source
    ADD CONSTRAINT dict_message_source_pk PRIMARY KEY (id);


--
-- Name: dict_proc_identity_type dict_proc_identity_type_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dict_proc_identity_type
    ADD CONSTRAINT dict_proc_identity_type_pk PRIMARY KEY (id);


--
-- Name: entity_executed_scenario entity_executed_scenario_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entity_executed_scenario
    ADD CONSTRAINT entity_executed_scenario_pk PRIMARY KEY (id);


--
-- Name: entity_scenario_content entity_scenario_content_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entity_scenario_content
    ADD CONSTRAINT entity_scenario_content_pk PRIMARY KEY (id);


--
-- Name: entity_scenario_identifier entity_scenario_identifier_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entity_scenario_identifier
    ADD CONSTRAINT entity_scenario_identifier_pk PRIMARY KEY (id);


--
-- Name: entity_scenario_message entity_scenario_message_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entity_scenario_message
    ADD CONSTRAINT entity_scenario_message_pk PRIMARY KEY (id);


--
-- Name: entity_scenario entity_scenario_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entity_scenario
    ADD CONSTRAINT entity_scenario_pk PRIMARY KEY (id);


--
-- Name: dict_message_dict_album_format dict_message_dict_album_format_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dict_message_dict_album_format
    ADD CONSTRAINT dict_message_dict_album_format_fk FOREIGN KEY (album_format_id) REFERENCES public.dict_album_format(id) ON UPDATE CASCADE;


--
-- Name: dict_message_dict_album_format dict_message_dict_album_format_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dict_message_dict_album_format
    ADD CONSTRAINT dict_message_dict_album_format_fk_1 FOREIGN KEY (message_id) REFERENCES public.dict_message(id) ON UPDATE CASCADE;


--
-- Name: dict_message_dict_exch_spec dict_message_dict_exch_spec_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dict_message_dict_exch_spec
    ADD CONSTRAINT dict_message_dict_exch_spec_fk FOREIGN KEY (exch_spec_id) REFERENCES public.dict_exch_spec(id) ON UPDATE CASCADE;


--
-- Name: dict_message_dict_exch_spec dict_message_dict_exch_spec_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dict_message_dict_exch_spec
    ADD CONSTRAINT dict_message_dict_exch_spec_fk_1 FOREIGN KEY (message_id) REFERENCES public.dict_message(id) ON UPDATE CASCADE;


--
-- Name: dict_message dict_message_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dict_message
    ADD CONSTRAINT dict_message_fk FOREIGN KEY (message_source_id) REFERENCES public.dict_message_source(id) ON UPDATE CASCADE;


--
-- Name: entity_executed_scenario entity_executed_scenario_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entity_executed_scenario
    ADD CONSTRAINT entity_executed_scenario_fk FOREIGN KEY (scenario_id) REFERENCES public.entity_scenario(id) ON UPDATE CASCADE;


--
-- Name: entity_scenario_content entity_scenario_content_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entity_scenario_content
    ADD CONSTRAINT entity_scenario_content_fk FOREIGN KEY (scenario_id) REFERENCES public.entity_scenario(id) ON UPDATE CASCADE;


--
-- Name: entity_scenario_content entity_scenario_content_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entity_scenario_content
    ADD CONSTRAINT entity_scenario_content_fk_1 FOREIGN KEY (message_id) REFERENCES public.dict_message(id) ON UPDATE CASCADE;


--
-- Name: entity_scenario entity_scenario_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entity_scenario
    ADD CONSTRAINT entity_scenario_fk FOREIGN KEY (album_format_id) REFERENCES public.dict_album_format(id) ON UPDATE CASCADE;


--
-- Name: entity_scenario entity_scenario_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entity_scenario
    ADD CONSTRAINT entity_scenario_fk_1 FOREIGN KEY (exch_spec_id) REFERENCES public.dict_exch_spec(id) ON UPDATE CASCADE;


--
-- Name: entity_scenario entity_scenario_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entity_scenario
    ADD CONSTRAINT entity_scenario_fk_2 FOREIGN KEY (declaration_mode_id) REFERENCES public.dict_declaration_mode(id) ON UPDATE CASCADE;


--
-- Name: entity_scenario_identifier entity_scenario_identifier_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entity_scenario_identifier
    ADD CONSTRAINT entity_scenario_identifier_fk FOREIGN KEY (executed_scenario_id) REFERENCES public.entity_executed_scenario(id) ON UPDATE CASCADE;


--
-- Name: entity_scenario_identifier entity_scenario_identifier_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entity_scenario_identifier
    ADD CONSTRAINT entity_scenario_identifier_fk_1 FOREIGN KEY (identity_type_id) REFERENCES public.entity_scenario_content(id) ON UPDATE CASCADE;


--
-- Name: entity_scenario_message entity_scenario_message_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entity_scenario_message
    ADD CONSTRAINT entity_scenario_message_fk FOREIGN KEY (message_type_id) REFERENCES public.dict_message(id) ON UPDATE CASCADE;


--
-- Name: entity_scenario_message entity_scenario_message_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entity_scenario_message
    ADD CONSTRAINT entity_scenario_message_fk_1 FOREIGN KEY (executed_scenario_id) REFERENCES public.entity_executed_scenario(id) ON UPDATE CASCADE;


--
-- Name: entity_scenario_message entity_scenario_message_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entity_scenario_message
    ADD CONSTRAINT entity_scenario_message_fk_2 FOREIGN KEY (step_id) REFERENCES public.entity_scenario_content(id) ON UPDATE CASCADE;


--
-- PostgreSQL database dump complete
--