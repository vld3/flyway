create role osved_emul_back;
grant insert on all tables in schema public to osved_emul_back;
grant update on all tables in schema public to osved_emul_back;
grant select on all tables in schema public to osved_emul_back;
grant delete on all tables in schema public to osved_emul_back;
grant osved_emul_back to osved_emul_dba;
alter role osved_emul_dba inherit;
