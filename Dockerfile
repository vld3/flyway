FROM adoptopenjdk/openjdk13:jdk-13.0.2_8-ubuntu-slim
RUN apt-get update
RUN apt-get install -y maven
COPY pom.xml /media/sf_sharedfolderubuntu/flyway/pom.xml
COPY src /media/sf_sharedfolderubuntu/flyway/src
WORKDIR /media/sf_sharedfolderubuntu/flyway
RUN mvn package
CMD ["java","-jar","target/flyway-0.0.1-SNAPSHOT.jar","com.example.flyway.FlywayApplication"]